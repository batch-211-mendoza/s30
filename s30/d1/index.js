/*
	AGGREGATION
	- Definition

	AGGREGATE METHODS
	1. Match
	2. Group
	3. Sort
	4. Project

	AGGREGATION PIPELINES

	OPERATORS
	1. Sum
	2. Max
	3. Min
	4. Avg
*/

// MongoDB AGGREGATION
/*
	- Used to generate manipulated data and perform operations to create filtered results that help in analyzing data
	- Compared to doing the CRUD Operations on our data from our previous sessions, aggregation gives us access to manipulate, filter, and compute for results, providing us with information to make nescessary development decisions.
	- Aggregations in MongoDB are very flexible and you cna form your own aggregation pipeline depending on the need of your application.
*/

// AGGREGATE METHODS
/*
	1. MATCH: "$match"
	- The "$match" is used to pass the documents that meet the specified condition(s) to the next pipeline stage/aggregation process.

	Syntax:
		db.collectionName.aggregate([
			{ $match: {field:value} }
		]);
		
*/
db.fruits.aggregate([
	{
		$match: 
		{
			onSale: true
		}
	}
]);

db.fruits.aggregate([
	{ 
		$match:
		{ 
			stock:
			{ 
				$gte: 20
			}
		}
	}
]);

/*
	2. GROUP: "$group"
	- The "$group" is used to group elements together field-value pairs using the data from the grouped element.

	Syntax:
		db.collectionName.aggregate([
			{
				$group: 
				{
					_id: "value", fieldResult: "valueResult"
				}
			}
		]);
*/
db.fruits.aggregate([
	{
		$group: 
		{
			_id: "$supplier_id", 
			total: 
			{
				$sum: "$stock"
			}
		}	
	}
]);

/*
	3. SORT: "$sort"
	- The "$sort" can be used when changing the order of aggregated results
	- Providing a value of -1 will sort the documents in descending order
	- Providing a value of 1 will sort the documents in ascending order

	Syntax:
		db.collectionName.aggregate([
			{
				$sort:
				{
					field: 1 / -1
				}
			}
		]);
*/
db.fruits.aggregate([
	{
		$match:
		{
			onSale: true
		}	
	},
	{
		$group:
		{
			_id: "$supplier_id",
			total:
			{
				$sum: "$stock"
			}
		}
	},
	{
		$sort:
		{
			total: -1
		}
	}
]);

/*
	4. PROJECT: "$project"
	- The "$project" can be used whe aggregating data to include or exclude fields from the returned results.

	Syntax:
		db.collectionName.aggregate([
			{ $project: {field: 1/0 } }
		]);
*/
db.fruits.aggregate([
	{
		$match:
		{
			onSale: true
		}
	},
	{
		$group:
		{
			_id: "$supplier_id",
			total:
			{
				$sum: "$stock"
			}
		}
	},
	{
		$project:
		{
			_id: 0
		}
	}
]);

db.fruits.aggregate([
	{
		$match:
		{
			onSale: true
		}
	},
	{
		$group:
		{
			_id: "$supplier_id",
			total:
			{
				$sum: "$stock"
			}
		}
	},
	{
		$project:
		{
			_id: 0
		}
	},
	{
		$sort:
		{
			total: 1
		}
	}
]);

// AGGREGATION PIPELINES
/*
	- The aggregation pipeline in MongoDB is a framework for data aggregation
	- Each stage transforms the documents as they pass through the deadlines

	Syntax:
		db.collectionName.aggregate([
			{staegA},
			{staegB},
			{staegC},
		]).
*/

// OPERATORS
	// 1. "$sum" - gets the total of everything
	db.fruits.aggregate([
		{
			$group:
			{
				_id: "$supplier_id",
				total:
				{
					$sum: "$stock"
				}
			}
		}
	]);

	// 2. "$max" - gets the highest value our of everything else
	db.fruits.aggregate([
		{
			$group:
			{
				_id: "$supplier_id",
				max_price:
				{
					$max: "$price"
				}
			}
		}
	]);

// 3. "$min" - gets the lowest value out of everything else
	db.fruits.aggregate([
		{
			$group:
			{
				_id: "$supplier_id",
				min_price:
				{
					$min: "$price"
				}
			}	
		}
	]);

// "$avg" - gets the average value of all the fields
	db.fruits.aggregate([
		{
			$group:
			{
				_id: "$supplier_id",
				avg_price: 
				{
					$avg: "$price"
				}
			}			
		}
	]);

/*
